use amiquip::{Connection, Exchange, Publish, Result};
use std::io;

fn main(){
    // Open connection.
    let mut connection = Connection::insecure_open("amqp://guest:guest@45.32.185.27:5672").unwrap();

    // Open a channel - None says let the library choose the channel ID.
    let channel = connection.open_channel(None).unwrap();

    // Get a handle to the direct exchange on our channel.
    let exchange = Exchange::direct(&channel);


    loop {
        let mut input = String::new();
        io::stdin().read_line(&mut input);
        input.pop();
        // Publish a message to the "hello" queue.
        exchange.publish(Publish::new(&input.as_bytes(), "hello")).unwrap();
    }
    
}