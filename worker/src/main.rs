use amiquip::{Connection, ConsumerMessage, ConsumerOptions, QueueDeclareOptions, Result};
use tokio::prelude::*;
use std::{thread, time};

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    
    //Our connection
    let mut connection = Connection::insecure_open("amqp://guest:guest@45.32.185.27:5672").unwrap();
    
    // Open a channel - None says let the library choose the channel ID.
    let channel = connection.open_channel(None)?;                                        //
                                                                                         //  This should be made with prefabricated
    // // Declare the "hello" queue.                                                     //  already known channels
    let queue = channel.queue_declare("hello", QueueDeclareOptions::default()).unwrap(); //
    
    // // Start a consumer.
    let consumer = queue.consume(ConsumerOptions::default())?;
    
    println!("Waiting for messages. Press Ctrl-C to exit.");

            for (i, message) in consumer.receiver().iter().enumerate() {
                match message {
                    ConsumerMessage::Delivery(delivery) => {
                        let delivered = delivery.clone();
                        let mut body = String::from_utf8(delivered.body).unwrap();

                        tokio::spawn(async move {
                            let delay_msg = String::from("yaba");
                            if delay_msg == body {
                                thread::sleep(time::Duration::from_millis(10000));
                            }
                            println!("({}) {}", i, &body);
                        });
                        consumer.ack(delivery).unwrap();
                    }
                    other => {
                        println!("Consumer ended: {:?}", other);
                    }
                }
            }
            
    Ok(())
}
